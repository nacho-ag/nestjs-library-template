## Dynamic module example

```javascript
@Module({})
export class ExampleModule {
  static forRoot({ foo, bar }: IExampleProps): DynamicModule {
    return {
      global: true,
      module: ExampleModule,
      imports: [HttpModule],
      controllers: [ExampleController],
      providers: [
        {
          provide: 'PROPS',
          useFactory: () => {
            foo, bar;
          },
        },
        ExampleService,
      ],
      exports: [ExampleService],
    };
  }
}
```
