import { Injectable } from '@nestjs/common';

@Injectable()
export class ExampleService {
  constructor() {}

  async hello() {
    return 'Hi';
  }
}
