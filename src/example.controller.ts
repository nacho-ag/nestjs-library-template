import { Controller, Get, Query } from '@nestjs/common';
import { ExampleService } from './example.service';

@Controller('config')
export class ExampleController {
  constructor(private readonly exampleService: ExampleService) {}

  @Get()
  Hello() {
    return this.exampleService.hello();
  }
}
